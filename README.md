# Flectra Community / vertical-hotel

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[report_hotel_restaurant](report_hotel_restaurant/) | 2.0.1.0.0| Restaurant Management - Reporting
[hotel_restaurant](hotel_restaurant/) | 2.0.1.0.0| Table booking facilities and Managing customers orders
[report_hotel_reservation](report_hotel_reservation/) | 2.0.1.0.0| Hotel Reservation Management - Reporting
[hotel_housekeeping](hotel_housekeeping/) | 2.0.1.0.0| Manages Housekeeping Activities and its Process
[hotel_reservation](hotel_reservation/) | 2.0.1.0.0| Manages Guest Reservation & displays Reservation Summary
[hotel](hotel/) | 2.0.1.0.0| Hotel Management to Manage Folio and Hotel Configuration


